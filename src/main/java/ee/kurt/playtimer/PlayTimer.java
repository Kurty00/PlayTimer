package ee.kurt.playtimer;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class PlayTimer extends JavaPlugin implements Listener, CommandExecutor {

    public static PlayTimer instance;
    public static HashMap<String, Integer> pdat = new HashMap<>();
   // public static HashMap<String, Integer> PM;
    FileConfiguration config = this.getConfig();

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        config.options().copyDefaults(true);
        pdat = loadHashMap(config);
        instance = this;
        getServer().getPluginManager().registerEvents(this, this);
        this.getCommand("resetplaytime").setExecutor(this::onCommand);
    }

    @EventHandler
    public void onJoin(final PlayerJoinEvent e) {

        Player p = e.getPlayer();
        Integer remainingSeconds = pdat.get(p.getUniqueId().toString());
        if(remainingSeconds == null || remainingSeconds < 0){
            pdat.put(p.getUniqueId().toString(), config.getInt("maxOnlineTime"));
            remainingSeconds = config.getInt("maxOnlineTime");
        }

        p.sendMessage("§eDu hast noch %s Spielzeit für heute.".replaceAll("%s", remainingSeconds/60 + " Minuten"));
        if(p.isOp() && config.getBoolean("opBypass")){
            p.sendMessage("§cDa du ein OP bist, darfst du trotzdem weiterspielen.");
        }
        if(remainingSeconds == 0 && (!p.isOp() && config.getBoolean("opBypass"))){
            p.kickPlayer("§cDeine Spielzeit ist abgelaufen.");
        }

        new BukkitRunnable(){
            @Override
            public void run() {
                Integer rs = pdat.get(p.getUniqueId().toString());
                if (rs == null) {
                    rs = config.getInt("maxOnlineTIme");
                }

                if (!p.isOnline()) {
                    rs = rs + 5;
                    pdat.put(p.getUniqueId().toString(), rs);
                    cancel();
                    return;
                }
                if (rs > 0) {
                    rs = rs - 5;
                }
                if (rs < 0) {
                    rs = 0;
                }
                if(rs == 60){
                    p.sendMessage("§cDu hast noch eine Minute Spielzeit für heute.");
                }
                if(rs == 30){
                    p.sendMessage("§cDu hast noch 30 Sekunden Spielzeit für heute.");
                }
                if(rs == 15){
                    p.sendTitle("§c15 Sekunden", "Spielzeit verbleibend.");
                }
                Integer min = rs/60;
                Integer sec = rs%60;
                if(rs%120 == 0 && config.getBoolean("enableActionbar")) {
                    p.sendActionBar("§cVerbleibende Spielzeit: " + min + " Minuten " + sec + " Sekunden");
                }
                pdat.put(p.getUniqueId().toString(), rs);
                saveHashMap(pdat);
                if (rs == 0 && (!p.isOp() && config.getBoolean("opBypass"))) {
                    cancel();
                    p.kickPlayer("§cDeine Spielzeit ist abgelaufen.");
                }
            }
        }.runTaskTimer(this, 0L, 5L*20L);

    }

    @EventHandler
    public void onLeave(final PlayerQuitEvent e) {
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender.isOp() || !(sender instanceof Player)){
            sender.sendMessage("Ok. Die Spielzeit wurde zurückgesetzt.");
            for (String key : pdat.keySet()) {
                pdat.put(key, config.getInt("maxOnlineTime"));
                config.set("playerdata." + key, config.getInt("maxOnlineTime"));
            }
            saveConfig();
            pdat = loadHashMap(config);
        }
        return true;
    }

    public HashMap<String, Integer> loadHashMap(FileConfiguration c) {
        HashMap<String, Integer> hm = new HashMap<>();
        ConfigurationSection cdat = c.getConfigurationSection("playerdata");
        if(cdat != null){
            for (String key : cdat.getKeys(false)) {
                hm.put(key, c.getInt("playerdata."+key));
            }
        }
        return hm;
    }

    public void saveHashMap(HashMap<String, Integer> hm) {
        for (String key : hm.keySet()) {
            config.set("playerdata." + key, hm.get(key));
        }

        //important: save the config!
        saveConfig();
    }

}
